const number_of_asteroids = 20;

var canvas = document.createElement("canvas");
var context;
var player;
var interval;

//koriste za odredivanje smjera kretanja igraca
var left = 0;
var up = 0;
var right = 0;
var down = 0;
const player_speed = 7;
const timerStart = Date.now();

var asteroids = [];
const greyColors = ["#787878","#DCDCDC", "#606060", "#D3D3D3","#404040","#C8C8C8","#C0C0C0","#BEBEBE","#B8B8B8"]; //nijanse sive koje asteroidi mogu poprimiti

function start() {
    canvas.id = "myGameCanvas";
    canvas.height = window.innerHeight - 18;
    canvas.width = window.innerWidth - 18;

    context = canvas.getContext("2d");
    context.shadowBlur = 20;
    context.shadowColor = "black";

    document.body.appendChild(canvas);

    //stvaranje igraca
    player = new Player();
    document.addEventListener("keydown", (event) => {
        playerMovement(event.code, player_speed);
    });
    document.addEventListener("keyup", (event) => {
        playerMovement(event.code, 0);
    });

    //stvaranje asteroida
    for (let i = 0 ; i < number_of_asteroids ; i++) {
        asteroids.push(new Asteroid());
    }

    interval = setInterval(drawNextFrame, 20);
}

function stop() {
    clearInterval(interval);
}

function clear() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

class Player {
    constructor() {
        this.x = canvas.width / 2;
        this.y = canvas.height / 2;
        this.w_h = 50; //w_h je duljina stranice kvadrata, w_h = width_height
    }

    get X() {
        return this.x;
    }

    get Y() {
        return this.y;
    }

    get W_H() {
        return this.w_h;
    }

    update() {
        context.save();
        context.fillStyle = "red";
        context.fillRect(this.x - this.w_h / 2 , this.y - this.w_h / 2, this.w_h, this.w_h);
        context.restore();
    }

    newPos() {
        //vrijednosti left, right, up i down mjenjaju listeneri za tipke
        this.x = this.x - left + right; 
        this.y = this.y - up + down;

        if (this.x - this.w_h / 2 < 0) {
            this.x = this.w_h / 2;
        } else if (this.x + this.w_h / 2 > canvas.width) {
            this.x = canvas.width - this.w_h / 2;
        }

        if (this.y - this.w_h / 2 < 0) {
            this.y = this.w_h / 2;
        } else if (this.y + this.w_h / 2 > canvas.height) {
            this.y = canvas.height - this.w_h / 2;
        }
    }
}

class Asteroid {
    constructor() {
        this.createAsteroid();
    }

    get X() {
        return this.x;
    }

    get Y() {
        return this.y;
    }

    get W_H() {
        return this.w_h;
    }

    //ova metoda je zaduzena za stvaranje parametara samog asteroida
    createAsteroid() {
        this.color = greyColors[Math.floor(Math.random() * greyColors.length)];
        this.w_h = player.W_H * (0.6 + Math.random());

        //odreduje se koliko će asteroid biti udaljen od okvira ekrana prilikom stvaranja
        let dist_from_edge = (Math.max(canvas.width, canvas.height) / 4) * (Math.random() + 1);
        
        //odreduje s koje strane asteroid dolazi 1:lijeve, 2:desne, 3:odozgo, 4:odozdo
        //takoder se odreduju njegove pocetne koordinate
        let strana = Math.floor(Math.random() * 4) + 1;
        switch (strana) {
            case 1:
                this.x = -dist_from_edge - this.w_h/2;
                this.y = canvas.height * Math.random();
                break;
            case 2:
                this.x = canvas.width + dist_from_edge + this.w_h/2;
                this.y = canvas.height * Math.random();
                break;
            case 3:
                this.x = canvas.width * Math.random();
                this.y = -dist_from_edge - this.w_h/2;
                break;
            case 4:
                this.x = canvas.width * Math.random();
                this.y = canvas.height + dist_from_edge + this.w_h/2;
                break;
        }

        //bira se nasumicna tocka unutar vidljivog okvira
        let x_dest = canvas.width * Math.random();
        let y_dest = canvas.height * Math.random();

        //odreduje se pravac kojim se krece y = kx + l, pravac prolazi asteroidovom pocetnom tockom, i nasumicno stvorenom odmah iznad
        this.k = 1.0 * (y_dest - this.y) / (x_dest - this.x);
        this.l = (this.y - this.k * this.x)

        //specificira je li se asteroid po koordinat x krece lijevo ili desno
        this.x_vector = player_speed;
        if (x_dest < this.x) {
            this.x_vector *= -1;
        }
        //smanjuje brzinu u smjeru x ako je nagib veci od jedan,
        if (Math.abs(this.k) > 1) {
            this.x_vector /= Math.abs(this.k);
        }
        //randomizira brzinu asteroida u odnosu na igraca
        this.x_vector *= (0.7 + Math.random());

    }

    update() {
        context.save();
        context.fillStyle = this.color;
        context.fillRect(this.x - this.w_h / 2 , this.y - this.w_h / 2, this.w_h, this.w_h);
        context.restore();
    }

    newPos() {
        let old_y = this.y;

        this.x = this.x + this.x_vector;
        this.y = this.k * this.x + this.l;

        //ako asteroid izade iz okvira canvasa koji se vidi, on se resetira
        //prakticki se stvara novi, ali ukupan broj asteroida je uvijek isti
        if (this.x - this.w_h > canvas.width && this.x_vector > 0
            || this.x + this.w_h < 0 && this.x_vector < 0
            || this.y + this.w_h < 0 && this.y < old_y
            || this.y - this.w_h > canvas.height && this.y > old_y) {
                this.createAsteroid();
        }
    }
}

function playerMovement(key, value) {
    switch (key) {
        case "ArrowLeft":
            left = value;
            break;
        case "ArrowUp":
            up = value;
            break;
        case "ArrowRight":
            right = value;
            break;
        case "ArrowDown":
            down = value;
            break;
    }
}

function drawNextFrame() {
    clear();

    player.newPos();
    player.update();

    for (let i = 0 ; i < number_of_asteroids ; i++) {
        asteroids[i].newPos();
        asteroids[i].update();
    }

    //provjera kolizije
    for (let i = 0 ; i < number_of_asteroids ; i++) {
        let dist_x = Math.abs(player.X - asteroids[i].X);
        let dist_y = Math.abs(player.Y - asteroids[i].Y);
        let min_allowed_dist = player.W_H / 2 + asteroids[i].W_H / 2;

        if (dist_x < min_allowed_dist
            && dist_y < min_allowed_dist) {
            stop();

            let timeEnd = Date.now();
            let result = timeEnd - timerStart;

            let temp = result;

            let milis = temp % 1000;
            temp = Math.floor(temp / 1000);
            milis = [milis < 100 ? "0" : "", milis < 10 ? "0" : "", milis];

            let seconds = temp % 60;
            temp = Math.floor(temp / 60);
            seconds = [seconds < 10 ? "0" : "", seconds];

            let minutes = temp;
            minutes = [minutes < 10 ? "0" : "", minutes];

            //provjera rezultata
            if (!localStorage.recordMilis || Number(localStorage.recordMilis) < result) {
                let recordFormat = minutes.join("") + ":" + seconds.join("") + ":" + milis.join("");
                localStorage.recordFormat = recordFormat;
                localStorage.recordMilis = result;
                alert("Novi rekord: " + recordFormat);
            } else {
                alert("Rezultat: " + minutes.join("") + ":" + seconds.join("") + ":" + milis.join("") + "\nRecord is: " + localStorage.recordFormat);
            }

            break;
        }
    }
}
